package es.upm.dit.apsv.traceconsumer2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.stereotype.Component;
import org.springframework.boot.CommandLineRunner;

import java.util.function.Consumer;

import es.upm.dit.apsv.traceconsumer2.model.*;
import es.upm.dit.apsv.traceconsumer2.Repository.*;

@SpringBootApplication
public class Traceconsumer2Application {

	public static void main(String[] args) {
		SpringApplication.run(Traceconsumer2Application.class, args);
	}

    public static final Logger log = LoggerFactory.getLogger(Traceconsumer2Application.class);

	@Autowired
    private  TraceRepository tr;

    @Autowired
    private  TraceOrderRepository tor;

    @Autowired
    private  Environment env;

	@Bean("consumer")
    public Consumer<Trace> checkTrace() {
            return t -> {
                    t.setTraceId(t.getTruck() + t.getLastSeen());
                    tr.save(t);
                    String uri = env.getProperty("orders.server");
                    TransportationOrder result = tor.findById(t.getTruck()).orElse(new TransportationOrder());
                    if (result != null && result.getTruck()!= null && ! result.getTruck().equals("") && result.getSt() == 0) {
                            result.setLastDate(t.getLastSeen());
                            result.setLastLat(t.getLat());
                            result.setLastLong(t.getLng());
                            if (result.distanceToDestination() < 10)
                                    result.setSt(1);
                            tor.save(result);
                            log.info("Order updated: "+ result);
                    }
                };
        }

    @Component
    class DemoCommandLineRunner implements CommandLineRunner{

        @Autowired
        private TransportationOrderRepository torderRepository;

        @Override
        public void run(String... args) throws Exception {

            TransportationOrder t = new TransportationOrder();
            t.setToid("MATRICULA");
            t.setTruck("MATRICULA"+ System.currentTimeMillis());
            t.setOriginDate(100000000);
            t.setDstDate(t.getOriginDate() + (1000*60*12));
            t.setOriginLat(0.0);
            t.setOriginLong(0);
            t.setDstLat(44);
            t.setDstLong(88);
            t.setSt(0);
            torderRepository.save(t);

        }
    }
}
